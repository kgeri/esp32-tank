#!/bin/bash

target=$1

if [ target == "esp32cam" ]; then
    if [ -z "$IDF_PATH" ]; then
        . /media/tools/esp-idf/export.sh
    fi
fi

gzip -fk -- resources/index.html
xxd -i resources/index.html.gz > resources/site.h

pio run --environment $target
