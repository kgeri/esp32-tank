#pragma once
#include <stdint.h>

#ifdef ARDUINO_ESP32_DEV
void motor_init(void);
void motor_set_speed(int8_t left_speed, int8_t right_speed);

#else  // native
void motor_init(void) {}
void motor_set_speed(int8_t left_speed, int8_t right_speed) {
    printf("motor: left=%d, right=%d\n", left_speed, right_speed);
}
#endif
