#pragma once

#ifdef ARDUINO_ESP32_DEV
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define HANDLER_RT void  // On FreeRTOS task handlers are void
#define HANDLER_EXIT   \
    vTaskDelete(NULL); \
    return  // On FreeRTOS we must delete the task before return
#define start_task(name, handler_method, handler_args) xTaskCreate(handler_method, #name, 4096, handler_args, 5, NULL)

#else  // native
#include <pthread.h>

#define HANDLER_RT void*          // On native pthread handlers return void*
#define HANDLER_EXIT return NULL  // On native, we can just return from the pthread handler
#define start_task(name, handler_method, handler_args) \
    pthread_t name##_thread;                           \
    pthread_create(&name##_thread, NULL, handler_method, (void*)handler_args)
#endif
