#pragma once

#ifdef ARDUINO_ESP32_DEV
#include "lwip/sockets.h"

#else
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <unistd.h>
#endif
