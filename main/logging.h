#pragma once

#ifdef ARDUINO_ESP32_DEV
#include "esp_log.h"
#define INFO(tag, format, ...) ESP_LOGI(tag, format, ##__VA_ARGS__)
#define ERROR(tag, format, ...) ESP_LOGE(tag, format, ##__VA_ARGS__)

#else  // native

#include <stdio.h>
#define INFO(tag, format, ...) printf("[INFO ] %s:", tag), printf(format, ##__VA_ARGS__), printf("\n")
#define ERROR(tag, format, ...) printf("[ERROR] %s:", tag), printf(format, ##__VA_ARGS__), printf("\n")
#endif
