#include <string.h>

#include "../resources/site.h"
#include "camera.h"
#include "errno.h"
#include "logging.h"
#include "motor.h"
#include "sockets.h"
#include "tasks.h"

typedef void (*SocketHandler)(const char *, int);  // name, sock_fd

typedef struct ServerTask {
    const char *name;       // The name of the server task, used for logging
    int port;               // The port to bind to
    SocketHandler handler;  // The method to read from and write to the socket
} ServerTask;

static HANDLER_RT tcp_server(void *params) {
    ServerTask *task = (ServerTask *)params;
    const char *tag = task->name;

    int keepalive = 1;     // Enable keepalive
    int keepIdle = 10;     // Send keepalive probe after N seconds of inactivity
    int keepInterval = 5;  // Send keepalive probe every N seconds after inactivity
    int keepCount = 10;    // Drop the connection after this many keepalive probes

    struct sockaddr_storage dest_addr;
    struct sockaddr_in *dest_addr_ip4 = (struct sockaddr_in *)&dest_addr;

    dest_addr_ip4->sin_addr.s_addr = htonl(INADDR_ANY);
    dest_addr_ip4->sin_family = AF_INET;
    dest_addr_ip4->sin_port = htons(task->port);

    // Creating the socket and setting options
    int listen_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_sock < 0) {
        ERROR(tag, "Unable to create socket: %d", errno);
        HANDLER_EXIT;
    }

    int opt = 1;
    setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    INFO(tag, "Socket created: %d", listen_sock);

    // Binding the socket
    int err = bind(listen_sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
    if (err != 0) {
        ERROR(tag, "Failed to bind %d: %d", task->port, errno);
        goto CLEAN_UP;
    }
    INFO(tag, "Socket bound: %d", task->port);

    // Listening on the socket
    err = listen(listen_sock, 1);
    if (err != 0) {
        ERROR(tag, "Failed to listen: %d", errno);
        goto CLEAN_UP;
    }

    while (1) {
        INFO(tag, "Listening on: %d", task->port);

        // Accepting incoming connections
        struct sockaddr_storage source_addr;
        socklen_t addr_len = sizeof(source_addr);
        int sock = accept(listen_sock, (struct sockaddr *)&source_addr, &addr_len);
        if (sock < 0) {
            ERROR(tag, "Failed to accept: %d", errno);
            break;
        }

        // Set socket options
        setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, &keepalive, sizeof(int));
        setsockopt(sock, IPPROTO_TCP, TCP_KEEPIDLE, &keepIdle, sizeof(int));
        setsockopt(sock, IPPROTO_TCP, TCP_KEEPINTVL, &keepInterval, sizeof(int));
        setsockopt(sock, IPPROTO_TCP, TCP_KEEPCNT, &keepCount, sizeof(int));
        char *addr_str = inet_ntoa(((struct sockaddr_in *)&source_addr)->sin_addr);
        INFO(tag, "Accepted: %s", addr_str);

        // Protocol handler
        task->handler(tag, sock);

        INFO(tag, "Closing: %s", addr_str);
        shutdown(sock, 0);
        close(sock);
    }

CLEAN_UP:
    close(listen_sock);
    HANDLER_EXIT;
}

const char HEADER[] =
    "HTTP/1.1 200 OK\r\n"
    "Access-Control-Allow-Origin: *\r\n"
    "Content-Type: multipart/x-mixed-replace; boundary=123456789000000000000987654321\r\n";
const char BOUNDARY[] = "\r\n--123456789000000000000987654321\r\n";
const char CTNTTYPE[] = "Content-Type: image/jpeg\r\nContent-Length: ";

static void cam_handler(const char *tag, int sock) {
    send(sock, HEADER, strlen(HEADER), 0);
    send(sock, BOUNDARY, strlen(BOUNDARY), 0);

    char buf[32];
    while (1) {
        FrameBuffer *fb = cam_fb_get();
        if (fb->len == 0) {
            continue;  // TODO wait a bit here?
        }

        int len = fb->len;
        sprintf(buf, "%d\r\n\r\n", len);

        send(sock, CTNTTYPE, strlen(CTNTTYPE), 0);
        send(sock, buf, strlen(buf), 0);
        send(sock, fb->buf, len, 0);
        cam_fb_release(fb);

        int sent = send(sock, BOUNDARY, strlen(BOUNDARY), 0);
        if (sent < 0) {
            ERROR(tag, "Failed to send: %d", errno);
            return;
        }

        usleep(50000);  // 50 ms == 20 fps
    }
}

// Supported requests:
//
// GET / HTTP/1.1 => index.html.gz
// GET /motor?left=...&right=... HTTP/1.1 => motor control
static void http_handler(const char *tag, int sock) {
    static char req_buf[2048];
    static char rsp_buf[1024];

    int len = recv(sock, &req_buf, sizeof(req_buf) - 1, 0);
    if (len <= 0) {
        ERROR(tag, "Recv returned: %d", len);
        return;
    }
    req_buf[len] = '\0';  // Ensure req_buf's a string
    rsp_buf[0] = '\0';    // Clear rsp_buf

    if (strncmp(req_buf, "GET /motor", 10) == 0) {
        // Motor control
        const char *body = strstr(req_buf, "/motor?");
        if (body == NULL) {
            ERROR(tag, "GET params not found in: %s", req_buf);
            sprintf(rsp_buf, "HTTP/1.1 400 Bad Request\r\n");
            goto SEND;
        }

        int left_speed, right_speed;
        if (!sscanf(body, "/motor?left=%4d&right=%4d", &left_speed, &right_speed)) {
            ERROR(tag, "Failed to parse: %s", body);
            sprintf(rsp_buf, "HTTP/1.1 400 Bad Request\r\n");
            goto SEND;
        }

        motor_set_speed(left_speed, right_speed);
        sprintf(rsp_buf, "HTTP/1.1 200 OK\r\n");
    } else if (strncmp(req_buf, "GET / HTTP/1.1", 14) == 0) {
        // index.html.gz
        sprintf(rsp_buf,
                "HTTP/1.1 200 OK\r\n"
                "Content-Type: text/html\r\n"
                "Content-Encoding: gzip\r\n"
                "Content-Length: %d\r\n"
                "\r\n",
                resources_index_html_gz_len);
        send(sock, rsp_buf, strlen(rsp_buf), 0);
        send(sock, resources_index_html_gz, resources_index_html_gz_len, 0);
    } else {
        ERROR(tag, "Unsupported request: %s", req_buf);
        sprintf(rsp_buf, "HTTP/1.1 400 Bad Request\r\n");
    }

SEND:
    send(sock, rsp_buf, strlen(rsp_buf), 0);  // TODO retransmit, error checking?
}

void server_start(void) {
    // Intializing the motor
    motor_init();

    // Initializing the camera
    cam_init();

    // Starting the HTTP handler
    static ServerTask http_task = {"http", HTTP_PORT, http_handler};
    start_task(http, tcp_server, &http_task);

    // Starting the camera server handler
    static ServerTask cam_task = {"cam-server", 3000, cam_handler};
    start_task(cam, tcp_server, &cam_task);
}
