#pragma once
#include <stddef.h>
#include <stdint.h>

typedef struct FrameBuffer {
    uint8_t* buf;
    uint32_t len;      // Length of buf
    uint32_t width;    // Width of the picture in pixels
    uint32_t height;   // Height of the picture in pixels
    const void* _ref;  // Pointer to the implementation-specific frame buffer
} FrameBuffer;

static uint8_t empty_buf[0];
static FrameBuffer EMPTY_FB = {
    .buf = empty_buf,
    .len = 0,
    .width = 0,
    .height = 0,
    ._ref = NULL,
};

#ifdef ARDUINO_ESP32_DEV
void cam_init(void);
FrameBuffer* cam_fb_get(void);
void cam_fb_release(FrameBuffer*);

#else  // native
void cam_init(void) {}
FrameBuffer* cam_fb_get(void) {
    return &EMPTY_FB;
}
void cam_fb_release(FrameBuffer* fb) {}
#endif
