#include "server.h"

#ifdef ARDUINO_ESP32_DEV
#include "esp32cam/wifi.h"

void app_main(void) {
    wifi_init();
    server_start();
}

#else  // native
#include <locale.h>
#include <pthread.h>

int main(void) {
    setlocale(LC_ALL, "C");
    server_start();
    pthread_exit(NULL);  // To avoid main exiting the entire process
}

#endif