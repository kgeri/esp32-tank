#include "camera.h"

#include "esp_camera.h"
#include "esp_log.h"

#define CAMERA_TAG "camera"

// ESP32Cam (AiThinker) PIN Map (OV2640 V2.0)
static camera_config_t camera_config = {
    .pin_pwdn = 32,
    .pin_reset = -1,  // Software reset will be performed
    .pin_xclk = 0,
    .pin_sscb_sda = 26,
    .pin_sscb_scl = 27,

    .pin_d7 = 35,
    .pin_d6 = 34,
    .pin_d5 = 39,
    .pin_d4 = 36,
    .pin_d3 = 21,
    .pin_d2 = 19,
    .pin_d1 = 18,
    .pin_d0 = 5,
    .pin_vsync = 25,
    .pin_href = 23,
    .pin_pclk = 22,

    .xclk_freq_hz = 20000000,  // XCLK 20MHz or 10MHz for OV2640 double FPS (Experimental)
    .ledc_timer = LEDC_TIMER_0,
    .ledc_channel = LEDC_CHANNEL_0,

    .pixel_format = PIXFORMAT_JPEG,  // YUV422,GRAYSCALE,RGB565,JPEG
    .frame_size = FRAMESIZE_SVGA,    // QQVGA-UXGA Do not use sizes above QVGA when not JPEG

    .jpeg_quality = 12,  // 0-63 lower number means higher quality
    .fb_count = 2,       // If more than one, i2s runs in continuous mode. Use only with JPEG
};

void cam_init(void) {
    esp_err_t err = esp_camera_init(&camera_config);
    if (err != ESP_OK) {
        ESP_LOGE(CAMERA_TAG, "Camera init failed: 0x%x", err);
        // TODO explicit panic here? (it'll panic when we try to esp_camera_fb_get anyway)
    }
}

FrameBuffer *cam_fb_get(void) {
    static FrameBuffer fb;

    camera_fb_t *cfb = esp_camera_fb_get();
    if (cfb == NULL) {
        ESP_LOGE(CAMERA_TAG, "Frame buffer null");
        return &EMPTY_FB;
    }

    ESP_LOGD(CAMERA_TAG, "Frame Buffer: %dx%d, %d bytes", cfb->width, cfb->height, cfb->len);
    fb.buf = cfb->buf;
    fb.len = cfb->len;
    fb.width = cfb->width;
    fb.height = cfb->height;
    fb._ref = cfb;
    return &fb;
}

void cam_fb_release(FrameBuffer *fb) {
    if (fb->_ref == NULL) return;
    esp_camera_fb_return((camera_fb_t *)fb->_ref);
}