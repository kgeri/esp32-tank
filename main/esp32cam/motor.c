#include <math.h>

#include "../logging.h"
#include "driver/mcpwm.h"

#define MOTOR_TAG "motor"

/** 
 * |        L298N-MOD            |
 * |                             |
 * '----(EA)(I1)(I2)(I3)(I4)(EB)-'
 *          \     |    /   /
 * .----(12)(13)(15)(14)(02)(04)-.
 * |                             |
 * |        ESP32-CAM            |
 * 
 * EA - Enable motor A (PWM)
 * I1 - When high (and I2 low), rotate A backward
 * I2 - When high (and I1 low), rotate A forward
 * I3 - When high (and I3 low), rotate B backward
 * I4 - When high (and I4 low), rotate B forward
 * EB - Enable motor B (PWM)
 */

#define LEFT_MOTOR_SPEED 2
#define LEFT_MOTOR_FWD 14
#define LEFT_MOTOR_BCK -1

#define RIGHT_MOTOR_SPEED 13
#define RIGHT_MOTOR_FWD 15
#define RIGHT_MOTOR_BCK -1

#define ESP_ASSERT(gpio_call)                                   \
    {                                                           \
        esp_err_t err = gpio_call;                              \
        if (err != ESP_OK) {                                    \
            ERROR(MOTOR_TAG, "Failed " #gpio_call ": %d", err); \
        }                                                       \
    }

void motor_init(void) {
    // GPIO setup
    ESP_ASSERT(gpio_set_direction(LEFT_MOTOR_FWD, GPIO_MODE_OUTPUT));
    ESP_ASSERT(gpio_set_level(LEFT_MOTOR_FWD, 0));
    // gpio_set_direction(LEFT_MOTOR_BCK, GPIO_MODE_OUTPUT);

    ESP_ASSERT(gpio_set_direction(RIGHT_MOTOR_FWD, GPIO_MODE_OUTPUT));
    ESP_ASSERT(gpio_set_level(RIGHT_MOTOR_FWD, 0));
    // gpio_set_direction(RIGHT_MOTOR_BCK, GPIO_MODE_OUTPUT);

    // PWM setup
    mcpwm_pin_config_t pin_config = {
        .mcpwm0a_out_num = LEFT_MOTOR_SPEED,
        .mcpwm1a_out_num = RIGHT_MOTOR_SPEED,
    };
    ESP_ASSERT(mcpwm_set_pin(MCPWM_UNIT_0, &pin_config));

    mcpwm_config_t pwm_config = {
        .frequency = 1000,
        .cmpr_a = 0.0,
        .cmpr_b = 0.0,
        .duty_mode = MCPWM_DUTY_MODE_0,
        .counter_mode = MCPWM_UP_COUNTER,
    };
    ESP_ASSERT(mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config));
    ESP_ASSERT(mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_1, &pwm_config));

    INFO(MOTOR_TAG, "MCPWM initialized successfully");
}

static inline void motor_set_direction_pins(gpio_num_t fwd_pin, gpio_num_t bck_pin, float speed) {
    if (speed < 0) {
        gpio_set_level(fwd_pin, 0);
        // gpio_set_level(bck_pin, 1);
    } else if (speed > 0) {
        gpio_set_level(fwd_pin, 1);
        // gpio_set_level(bck_pin, 0);
    } else {
        gpio_set_level(fwd_pin, 0);
        // gpio_set_level(bck_pin, 0);
    }
}

void motor_set_speed(int8_t left_speed, int8_t right_speed) {
    float ls = left_speed * 100.0F / 128;
    float rs = right_speed * 100.0F / 128;

    INFO(MOTOR_TAG, "left=%f, right=%f", ls, rs);

    mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, fabsf(ls));
    mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, MCPWM_DUTY_MODE_0);
    motor_set_direction_pins(LEFT_MOTOR_FWD, LEFT_MOTOR_BCK, ls);

    mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_A, fabsf(rs));
    mcpwm_set_duty_type(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_OPR_A, MCPWM_DUTY_MODE_0);
    motor_set_direction_pins(RIGHT_MOTOR_FWD, RIGHT_MOTOR_BCK, rs);
}
